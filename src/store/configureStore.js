import { 
    createStore, 
    applyMiddleware, 
    combineReducers, 
    compose 
} from 'redux';
import thunkMiddleware from 'redux-thunk';
import progressReducer from '../reducers/progress.reducer';
import logger from 'redux-logger';

const rootReducer = combineReducers({
    progress: progressReducer
});

export default function configureStore() {
    return createStore(
        rootReducer,
        process.env.NODE_ENV === 'dev' ?
            compose(
                applyMiddleware(thunkMiddleware, logger),
                window.devToolsExtension ? window.devToolsExtension() : f => f,
            )
            : compose(
                applyMiddleware(thunkMiddleware),
                window.devToolsExtension ? window.devToolsExtension() : f => f,
            )
    )
}