export const serverBase = "http://pb-api.herokuapp.com";

export function fetchApi(url, config) {
    return fetch(`${serverBase}/${url}`, config).then(response => {
        return response.json();
    })
}