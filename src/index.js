import React, { lazy, Suspense } from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import * as serviceWorker from "./serviceWorker";
import { Provider } from 'react-redux';
import {
    BrowserRouter,
    Route,
    Switch,
    Redirect
} from "react-router-dom";
import configureStore from './store/configureStore';

const ProgressBarsModule = lazy(() => import('./modules/progress-bars/progress-bars.module'));
const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Suspense fallback={<div>Loading...</div>}>
                <Switch>
                    <Route path="/" component={ProgressBarsModule} />
                    <Redirect to="/" />
                </Switch>
            </Suspense>
        </BrowserRouter>
    </Provider>
, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
