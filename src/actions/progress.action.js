import { GET_PROGRESS_DATA_SUCCESS, UPDATE_PROGRESS_DATA, REQUEST_PROGRESS_DATA } from "../reducers/progress.reducer";

export function requestProgress() {
    return {
        type: REQUEST_PROGRESS_DATA,
    }
}

export function getProgressDataSuccess(data) {
    return {
        type: GET_PROGRESS_DATA_SUCCESS,
        payload: {
            data
        }
    }
}

export function updateProgress(data) {
    return {
        type: UPDATE_PROGRESS_DATA,
        payload: {
            data
        }
    }
}