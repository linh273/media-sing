import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "./bar.component.scss";

function BarComponent(props) {
    const [percent, setPercent] = useState(0);
    const [currentPercentValue, setCurrentPercentValue] = useState(0);

    useEffect(() => {
        setPercent(props.value <= 100 ? props.value: 100);
        setCurrentPercentValue(props.value > 0 ? props.value : 0);
    }, [props.value]);

    return(
        <div className="bar-container">
            <span 
                className={`progress ${currentPercentValue > 100 ? 'over-limit' : ''}`}
                style={{width: `${percent}%`}}></span>
            <span className="value" id={`value-${props.id}`}>
                {currentPercentValue} %
            </span>
        </div>
    )
}

export default BarComponent;

BarComponent.propTypes = {
    id: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
}

BarComponent.defaultProps = {
    id: -1,
    value: 0
}