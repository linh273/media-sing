import React from "react";
import BarComponent from "../../components/bar/bar.component";
import {
    mount
} from "enzyme";
import { MOCK_DATA } from "../../assets/mock/mock";

describe('BarComponent', () => {
    let wrapper;
    const index = 0;
    const getWrapper = (index) => mount(
        <BarComponent 
            id={index}
            value={Math.round(MOCK_DATA.bars[index] / MOCK_DATA.limit * 100)}
        />
    );

    beforeEach(() => {
        wrapper = getWrapper(index);       
    });

    it('should render bar component', () => {
        expect(wrapper.find('div.bar-container').length).toEqual(1);
    });

    it('renders the current value', () => {
        expect(wrapper.find(`#value-${index}`).text()).toEqual(`${MOCK_DATA.percents[index]} %`);
    });

    it('renders the percent of progress bar when value <= limit', () => {
        const style = wrapper.find('.progress').get(0).props.style;
        expect(style).toHaveProperty('width', `${MOCK_DATA.percents[index]}%`);
    });

    it('renders the percent of progress bar when value > limit', () => {
        const index = 1;
        wrapper = getWrapper(index);
        expect(wrapper.find(`#value-${index}`).text()).toEqual(`${MOCK_DATA.percents[index]} %`);
        expect(wrapper.find('.over-limit').length).toEqual(1);
    });
});