import React from "react";
import {
    shallow
} from "enzyme";
import LoaderComponent from "./loader.component";

describe('LoaderComponent', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <LoaderComponent/>
        ) 
    });

    it('should render loader component', () => {
        expect(
            wrapper.find('div.loader').length
        ).toEqual(1);
    });
});