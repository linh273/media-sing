import React from "react";
import "./loader.component.scss";

function LoaderComponent() {
    return (
        <div className="loader">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    )
}

export default LoaderComponent;