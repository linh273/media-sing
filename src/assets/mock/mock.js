export const MOCK_DATA = {
    limit: 220,
    buttons: [34, 30, -28, -33],
    bars: [32, 370, 200],
    percents: [15, 168, 91],
    zeroPercent: 0,
    overLimitPercent: 121
}
