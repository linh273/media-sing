import { MOCK_DATA } from "./mock";

export const REQUEST_PROGRESS_DATA = "REQUEST_PROGRESS_DATA";
export const GET_PROGRESS_DATA_SUCCESS = "GET_PROGRESS_DATA_SUCCESS";
export const UPDATE_PROGRESS_DATA = "UPDATE_PROGRESS";

const initialState = {
    isLoading: false,
    data: {
        limit: MOCK_DATA.limit,
        buttons: [...MOCK_DATA.buttons],
        bars: [...MOCK_DATA.bars]
    }
};

export default function progressMockReducer(state = initialState, action) {
    switch(action.type) {
        case REQUEST_PROGRESS_DATA: {
            return initialState;
        }
        case GET_PROGRESS_DATA_SUCCESS:
        case UPDATE_PROGRESS_DATA: {
            
            if(action.payload) {
                return {...action.payload, isLoading: false};
            }
            return {...state, isLoading: false};
        }
        default: {
            return initialState;
        }
    }
}
