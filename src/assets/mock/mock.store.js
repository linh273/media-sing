import { 
    createStore, 
    applyMiddleware, 
    combineReducers, 
    compose 
} from 'redux';
import thunkMiddleware from 'redux-thunk';
import progressMockReducer from './mock.reducer';

const rootReducer = combineReducers({
    progress: progressMockReducer
});


export default function configureStore() {
    return createStore(
        rootReducer,
        compose(
            applyMiddleware(thunkMiddleware),
            window.devToolsExtension ? window.devToolsExtension() : f => f,
        )
    )
}