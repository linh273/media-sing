import React from 'react';
import Enzyme, {
    mount
} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import {
    Provider
} from 'react-redux';
import ProgressBarsModule from './progress-bars.module';
import { MOCK_DATA } from '../../assets/mock/mock';
import BarComponent from '../../components/bar/bar.component';
import * as progressActions from '../../actions/progress.action';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import LoaderComponent from '../../components/loader/loader.component';
import configureStore from '../../assets/mock/mock.store';

Enzyme.configure({
    adapter: new EnzymeAdapter()
});

const mockStore = configureMockStore([thunk]);

describe('ProgressBarsModule', () => {
    let store = mockStore({
        progress: {
            isLoading: false,
            data: {...MOCK_DATA}
        }
    })
    let wrapper;

    const getWrapper = (store) => mount(
        <Provider store={store}>
          <ProgressBarsModule/>
        </Provider>
    );

    beforeEach(() => {
        store.clearActions();
        wrapper = getWrapper(store);
    });

    it('renders container class', () => {
        expect(wrapper.find('div.container').length).toEqual(1);
    });

    /* Loader Component */
    it('renders the LoaderComponent', () => {
        const store = mockStore({
            progress: {
                isLoading: true,
                data: MOCK_DATA
            }
        })
        wrapper = getWrapper(store);
        expect(wrapper.find(LoaderComponent).length).toEqual(1);
    });

    /* Bar Component */
    it('renders the BarComponent list', () => {
        expect(wrapper.find(BarComponent).length).toEqual(MOCK_DATA.bars.length);
    });

    it('renders specific BarComponent', () => {
        const index = 0;
        const barsGroup = wrapper.find('.bars-group');
        expect(barsGroup.contains(
            <BarComponent 
                key={index} 
                value={Math.round(MOCK_DATA.bars[index] / MOCK_DATA.limit * 100)} 
            />
        ))
    });

    /* Button Component */
    it('renders the button list', () => {
        expect(wrapper.find('.control-button')).toHaveLength(MOCK_DATA.buttons.length);
    });

    it('should DISPATCH to request progress data', () => {
        store.dispatch = jest.fn();
        wrapper = getWrapper(store);
        expect(store.dispatch).toHaveBeenCalledWith(progressActions.requestProgress());
    });

    it('should click button and DISPATCH correct update action', () => {
        const buttonIndex = 0;
        const selectedBar = 0;

        expect(wrapper.find(`#value-${selectedBar}`).text()).toEqual(`${MOCK_DATA.percents[selectedBar]} %`);
        store.dispatch = jest.fn();
        wrapper = getWrapper(store);
        wrapper.find(`#button-${buttonIndex}`).simulate('click');
        expect(store.dispatch).toHaveBeenCalledWith(progressActions.updateProgress(MOCK_DATA));
    });

    it('updates bars when click button: Cant go under 0, bar value + button value < 0', () => {
        const buttonIndex = 0;
        const selectedBar = 0;

        const store = configureStore();
        const wrapper = getWrapper(store);
        // bar: 15% button: -33
        expect(wrapper.find(`#value-${selectedBar}`).text()).toEqual(`${MOCK_DATA.percents[selectedBar]} %`);
        expect(wrapper.find(`#button-${buttonIndex}`).text()).toEqual(`${MOCK_DATA.buttons[buttonIndex]}`);
        wrapper.find(`#button-${buttonIndex}`).simulate('click');
        expect(wrapper.find(`#value-${selectedBar}`).text()).toEqual(`${MOCK_DATA.zeroPercent} %`);
    });

    it('updates bars when click button: bar value + button value > 0', () => {
        const buttonIndex = 2;
        const selectedBar = 2;

        const store = configureStore();
        const wrapper = getWrapper(store);

        wrapper.find('select').simulate('change',{target: { value : selectedBar }});
        // bar: 91%, button: 30
        expect(wrapper.find(`#value-${selectedBar}`).text()).toEqual(`${MOCK_DATA.percents[selectedBar]} %`);
        expect(wrapper.find(`#button-${buttonIndex}`).text()).toEqual(`+${MOCK_DATA.buttons[buttonIndex]}`);
        wrapper.find(`#button-${buttonIndex}`).simulate('click');
        expect(wrapper.find(`#value-${selectedBar}`).text()).toEqual(`${MOCK_DATA.overLimitPercent} %`);
    });
});