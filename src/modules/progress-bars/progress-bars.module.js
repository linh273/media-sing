import React, { useState, useEffect } from "react";
import "./progress-bars.scss";
import BarComponent from "../../components/bar/bar.component";
import { fetchApi } from "../../utils/api.utils";
import LoaderComponent from "../../components/loader/loader.component";
import { useDispatch, useSelector } from "react-redux";
import * as progressActions from '../../actions/progress.action';

function ProgressBarsModule() {
    const [selectedBar, setSelectedBar] = useState(0);
    const progressData = useSelector(state => state.progress);
    const dispatch = useDispatch();

    // NOTE: Just use redux way(show off). useState is more easier
    useEffect(() => {
        const callApi = () => {
            dispatch(progressActions.requestProgress());
            const config = {
                method: 'GET',
            }
            fetchApi("/bars", config).then(data => {
                dispatch(progressActions.getProgressDataSuccess(data));
            }).catch(error => {
                console.log(error);
            });
        }

        callApi();
    }, [dispatch]);

    const handleChangeSelect = (e) => {
        setSelectedBar(e.target.value || 0);
    }

    const handleClickButton = (value) => {
        const newValue = progressData.data.limit * value / 100 + progressData.data.bars[selectedBar];
        const bars = progressData.data.bars;
        bars[selectedBar] = newValue > 0 ? newValue : 0;
        const data = {
            ...progressData.data,
            bars
        }
        dispatch(progressActions.updateProgress(data));
    }
    
    return (
        <div className="container">
            <h1>Progress Bars Demo</h1>
            {
                progressData.isLoading ? <LoaderComponent /> :
                <div className="content">
                    <div className="bars-group">
                        { 
                            progressData.data.bars && progressData.data.bars.map((bar, index) => {
                                return <BarComponent 
                                            key={index}
                                            id={index}
                                            value={Math.round(bar / progressData.data.limit * 100)} 
                                        />
                            })
                        }
                    </div>
                    <div className="controls">
                        <select onChange={handleChangeSelect} className="select">
                            {
                                progressData.data.bars && progressData.data.bars.map((_, index) => {
                                    return <option 
                                                value={index}
                                                key={index}>#progress {index + 1}
                                            </option>
                                })
                            }
                        </select>
                        <div className="buttons">
                            {
                                progressData.data.buttons && progressData.data.buttons.sort((a, b) => (a - b)).map((button, index) => {
                                    return <button 
                                            key={index}
                                            id={`button-${index}`} 
                                            onClick={() => handleClickButton(button)} 
                                            className="control-button red">
                                            { button > 0 ? `+${button}` : button }
                                        </button>
                                })
                            }
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default ProgressBarsModule;