export const REQUEST_PROGRESS_DATA = "REQUEST_PROGRESS_DATA";
export const GET_PROGRESS_DATA_SUCCESS = "GET_PROGRESS_DATA_SUCCESS";
export const UPDATE_PROGRESS_DATA = "UPDATE_PROGRESS";

const initialState = {
    isLoading: false,
    data: {
        buttons: [],
        bars: [],
        limit: 0
    }
};

export default function progressReducer(state = initialState, action) {
    switch(action.type) {
        case REQUEST_PROGRESS_DATA: {
            return {...state, isLoading: true};
        }
        case UPDATE_PROGRESS_DATA:
        case GET_PROGRESS_DATA_SUCCESS: {
            if(action.payload) {
                return {...action.payload, isLoading: false};
            }
            return {...state, isLoading: false};
        }
        default: {
            return initialState;
        }
    }
}