### To start project

Step 1: `yarn build` to setup some dependencies<br />
Step 2: `yarn start` to run project.

### Testing

- Local: just run `yarn test` to see results.

### Build

- Using CI/CD on gitlab.
- After merging/pushing code into `master` (only this branch), the project will be deployed automatically.


### Structure
- assets
    - styles
    - mock
- components
    - component
    - specs
    - scss
- modules
    - module
    - scss
    - specs
- utils
- actions
- redux
- store
- constants

### Let see project directly

- Before getting it, please fix CORS error on `http://pb-api.herokuapp.com/bars`.
- Here is link `https://linh273.gitlab.io/media-sing/`.

Have fun !